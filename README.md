# Clean Insights Design

Core design, architecture, specs, etc.


## Main Specification

https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/docs/CleanInsights-ClientSpec-v0.0.1.md


## JSON Schemes

[JSON Schema Docs](./schema-docs/README.md)

JSON Schema: https://json-schema.org/understanding-json-schema/

Schema validated with https://www.jsonschemavalidator.net

Generated with https://github.com/adobe/jsonschema2md.

Usage:

```sh
npm install
npm run generate
```
