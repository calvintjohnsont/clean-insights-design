# README

## Top-level Schemas

*   [CleanInsights Matomo Proxy API](./cimp.md "The scheme defining the JSON API of the CleanInsights Matomo Proxy") – `https://cleaninsights.org/schemas/cimp.schema.json`

*   [CleanInsights SDK Configuration](./configuration.md "The scheme defining the JSON configuration file for CleanInsights SDK") – `https://cleaninsights.org/schemas/configuration.schema.json`

## Other Schemas

### Objects

*   [Event Measurement](./cimp-properties-event-measurement-event-measurement.md "A single aggregated measurement of a specific event") – `https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items`

*   [Measurement Campaigns](./configuration-properties-measurement-campaigns.md "Each insight you want to gain needs to be configured as a measurement campaign") – `https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns`

*   [Unique Campaign ID](./configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id.md "Every campaign needs a unique identifier, which you use as an argument to a measurement call, so that the measurement can be checked against the campaign parameters") – `https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$`

*   [Visit Measurement](./cimp-properties-visit-measurements-visit-measurement.md "A single aggregated measurement of repeated visits to a page/scene/activity") – `https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits/items`

### Arrays

*   [Event Measurement](./cimp-properties-event-measurement.md "List of aggregated measurements of a specific event") – `https://cleaninsights.org/schemas/cimp.schema.json#/properties/events`

*   [Visit Measurements](./cimp-properties-visit-measurements.md "List of aggregated measurements to specific pages/scenes/activities") – `https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits`

## Version Note

The schemas linked above follow the JSON Schema Spec version: `http://json-schema.org/draft/2019-09/schema#`
