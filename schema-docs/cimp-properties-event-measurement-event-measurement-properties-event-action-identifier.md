# Event Action Identifier Schema

```txt
https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/action
```

An action identifier for the Matomo event tracking: <https://matomo.org/docs/event-tracking/>

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                              |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [cimp.schema.json*](../schemas/cimp.schema.json "open original schema") |

## action Type

`string` ([Event Action Identifier](cimp-properties-event-measurement-event-measurement-properties-event-action-identifier.md))

## action Constraints

**minimum length**: the minimum number of characters for this string is: `1`

## action Examples

```json
"Play"
```

```json
"Pause"
```

```json
"Duration"
```

```json
"Add Playlist"
```

```json
"Downloaded"
```

```json
"Clicked"
```
