# Event Category Identifier Schema

```txt
https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/category
```

A category identifier for the Matomo event tracking: <https://matomo.org/docs/event-tracking/>

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                              |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [cimp.schema.json*](../schemas/cimp.schema.json "open original schema") |

## category Type

`string` ([Event Category Identifier](cimp-properties-event-measurement-event-measurement-properties-event-category-identifier.md))

## category Constraints

**minimum length**: the minimum number of characters for this string is: `1`

## category Examples

```json
"Videos"
```

```json
"Music"
```

```json
"Games"
```
