# Event Name Schema

```txt
https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/name
```

An action name for the Matomo event tracking: <https://matomo.org/docs/event-tracking/>

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                              |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [cimp.schema.json*](../schemas/cimp.schema.json "open original schema") |

## name Type

`string` ([Event Name](cimp-properties-event-measurement-event-measurement-properties-event-name.md))

## name Examples

```json
"Office Space"
```

```json
"Jonathan Coulton - Code Monkey"
```

```json
"kraftwerk-autobahn.mp3"
```
