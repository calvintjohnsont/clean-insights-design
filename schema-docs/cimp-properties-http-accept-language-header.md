# HTTP Accept-Language Header Schema

```txt
https://cleaninsights.org/schemas/cimp.schema.json#/properties/lang
```

A HTTP Accept-Language header. Matomo uses this value to detect the visitor's country.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                              |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [cimp.schema.json*](../schemas/cimp.schema.json "open original schema") |

## lang Type

`string` ([HTTP Accept-Language Header](cimp-properties-http-accept-language-header.md))

## lang Examples

```json
"fr-CH, fr;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5"
```

```json
"en"
```

```json
"de_AT"
```
