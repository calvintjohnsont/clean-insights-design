# End UNIX Epoch Timestamp Schema

```txt
https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits/items/properties/period_end
```

End of the aggregation period in seconds since 1970-01-01 00:00:00 UTC

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                              |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [cimp.schema.json*](../schemas/cimp.schema.json "open original schema") |

## period_end Type

`integer` ([End UNIX Epoch Timestamp](cimp-properties-visit-measurements-visit-measurement-properties-end-unix-epoch-timestamp.md))

## period_end Examples

```json
1602499451
```
