# Visit Measurements Schema

```txt
https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits
```

List of aggregated measurements to specific pages/scenes/activities.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                              |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [cimp.schema.json*](../schemas/cimp.schema.json "open original schema") |

## visits Type

`object[]` ([Visit Measurement](cimp-properties-visit-measurements-visit-measurement.md))
