# CIMP Server Address Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/server
```

The fully qualified URI to the CIMP (CleanInsights Matomo Proxy) endpoint, where the gathered measurements will be offloaded.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## server Type

`string` ([CIMP Server Address](configuration-properties-cimp-server-address.md))

## server Constraints

**URI**: the string must be a URI, according to [RFC 3986](https://tools.ietf.org/html/rfc3986 "check the specification")

## server Examples

```json
"https://matomo.example.org/ci/cleaninsights.php"
```
