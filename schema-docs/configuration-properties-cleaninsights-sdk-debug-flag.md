# CleanInsights SDK Debug Flag Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/debug
```

When set to true, the CleanInsights SDK will log a debugging information to the console.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## debug Type

`boolean` ([CleanInsights SDK Debug Flag](configuration-properties-cleaninsights-sdk-debug-flag.md))
