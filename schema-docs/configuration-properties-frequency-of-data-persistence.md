# Frequency of Data Persistence Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/persistEveryNTimes
```

Number of times of measurements after when the collected data is persisted again. Set to one to have the SDK persist the recorded data after every measurement. Set it to a very high number, to never persist. You will need to make sure, that persistence is done by you, otherwise you will loose data!

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## persistEveryNTimes Type

`integer` ([Frequency of Data Persistence](configuration-properties-frequency-of-data-persistence.md))

## persistEveryNTimes Constraints

**minimum**: the value of this number must greater than or equal to: `1`

## persistEveryNTimes Default Value

The default value is:

```json
10
```

## persistEveryNTimes Examples

```json
1
```

```json
10
```

```json
25
```

```json
99999999
```
