# Matomo Site ID Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/siteId
```

The site ID used in the Matomo server which will collect and analyze the gathered data.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## siteId Type

`integer` ([Matomo Site ID](configuration-properties-matomo-site-id.md))

## siteId Constraints

**minimum**: the value of this number must greater than or equal to: `1`

## siteId Examples

```json
1
```

```json
2
```

```json
3
```

```json
345345
```
