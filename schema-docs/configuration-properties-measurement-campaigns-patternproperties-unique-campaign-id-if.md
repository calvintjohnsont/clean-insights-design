# Untitled undefined type in CleanInsights SDK Configuration Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/if
```



| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | No           | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## if Type

unknown

# if Properties

| Property                                      | Type          | Required | Nullable       | Defined by                                                                                                                                                                                                                                                                                                 |
| :-------------------------------------------- | :------------ | :------- | :------------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [eventAggregationRule](#eventaggregationrule) | Not specified | Optional | cannot be null | [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-if-properties-eventaggregationrule.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/if/properties/eventAggregationRule") |

## eventAggregationRule



`eventAggregationRule`

*   is optional

*   Type: unknown

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-if-properties-eventaggregationrule.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/if/properties/eventAggregationRule")

### eventAggregationRule Type

unknown
