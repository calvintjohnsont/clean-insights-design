# Campaign Timeframe End Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/end
```

Campaigns run for a certain time. Measurements trying to be done for this campaign after this point in time will be ignored.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## end Type

`string` ([Campaign Timeframe End](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-campaign-timeframe-end.md))

## end Constraints

**date time**: the string must be a date time string, according to [RFC 3339, section 5.6](https://tools.ietf.org/html/rfc3339 "check the specification")

## end Examples

```json
"2018-11-13T20:20:39+00:00"
```

```json
"2020-12-31T23:59:59-05:00"
```
