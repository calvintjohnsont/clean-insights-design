# Campaign Timeframe Start Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/start
```

Campaigns run for a certain time. Measurements trying to be done for this campaign before this point in time will be ignored.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## start Type

`string` ([Campaign Timeframe Start](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-campaign-timeframe-start.md))

## start Constraints

**date time**: the string must be a date time string, according to [RFC 3339, section 5.6](https://tools.ietf.org/html/rfc3339 "check the specification")

## start Examples

```json
"2018-11-13T20:20:39+00:00"
```

```json
"2020-10-01T00:00:00-05:00"
```
