# Number of Periods Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/numberOfPeriods
```

The number of periods you want to measure in a row. Therefore the total length in days you measure one user is `aggregationPeriodLength * numberOfPeriods` beginning with the first day of the next period after the user consented.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## numberOfPeriods Type

`integer` ([Number of Periods](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-number-of-periods.md))

## numberOfPeriods Constraints

**minimum**: the value of this number must greater than or equal to: `1`

## numberOfPeriods Default Value

The default value is:

```json
1
```

## numberOfPeriods Examples

```json
1
```

```json
2
```

```json
3
```
