# Strengthen Anonymity Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/strengthenAnonymity
```

When set to true, measurements only ever start at the next full period. This ensures, that anonymity guaranties aren't accidentally reduced because the first period is very short.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## strengthenAnonymity Type

`boolean` ([Strengthen Anonymity](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-strengthen-anonymity.md))
