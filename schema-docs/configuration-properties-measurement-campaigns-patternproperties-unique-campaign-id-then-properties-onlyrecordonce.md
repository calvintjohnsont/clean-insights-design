# Untitled undefined type in CleanInsights SDK Configuration Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/then/properties/onlyRecordOnce
```



| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## onlyRecordOnce Type

unknown

## onlyRecordOnce Constraints

**constant**: the value of this property must be equal to:

```json
false
```
