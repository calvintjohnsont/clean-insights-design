# Untitled undefined type in CleanInsights SDK Configuration Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/then
```



| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | No           | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## then Type

unknown

# then Properties

| Property                          | Type          | Required | Nullable       | Defined by                                                                                                                                                                                                                                                                                         |
| :-------------------------------- | :------------ | :------- | :------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [onlyRecordOnce](#onlyrecordonce) | Not specified | Optional | cannot be null | [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-then-properties-onlyrecordonce.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/then/properties/onlyRecordOnce") |

## onlyRecordOnce



`onlyRecordOnce`

*   is optional

*   Type: unknown

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-then-properties-onlyrecordonce.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/then/properties/onlyRecordOnce")

### onlyRecordOnce Type

unknown

### onlyRecordOnce Constraints

**constant**: the value of this property must be equal to:

```json
false
```
