# Unique Campaign ID Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$
```

Every campaign needs a unique identifier, which you use as an argument to a measurement call, so that the measurement can be checked against the campaign parameters.

| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | No           | Forbidden         | Forbidden             | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## ^.+$ Type

`object` ([Unique Campaign ID](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id.md))

## ^.+$ Examples

```json
"my-special-campaign"
```

```json
"settingsUsage"
```

# ^.+$ Properties

| Property                                            | Type      | Required | Nullable       | Defined by                                                                                                                                                                                                                                                                                                         |
| :-------------------------------------------------- | :-------- | :------- | :------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [start](#start)                                     | `string`  | Required | cannot be null | [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-campaign-timeframe-start.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/start")                          |
| [end](#end)                                         | `string`  | Required | cannot be null | [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-campaign-timeframe-end.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/end")                              |
| [aggregationPeriodLength](#aggregationperiodlength) | `integer` | Required | cannot be null | [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-length-of-an-aggregation-period.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/aggregationPeriodLength") |
| [numberOfPeriods](#numberofperiods)                 | `integer` | Optional | cannot be null | [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-number-of-periods.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/numberOfPeriods")                       |
| [onlyRecordOnce](#onlyrecordonce)                   | `boolean` | Optional | cannot be null | [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-only-record-once-flag.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/onlyRecordOnce")                    |
| [eventAggregationRule](#eventaggregationrule)       | `string`  | Optional | cannot be null | [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-event-value-aggregation-rule.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/eventAggregationRule")       |
| [strengthenAnonymity](#strengthenanonymity)         | `boolean` | Optional | cannot be null | [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-strengthen-anonymity.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/strengthenAnonymity")                |

## start

Campaigns run for a certain time. Measurements trying to be done for this campaign before this point in time will be ignored.

`start`

*   is required

*   Type: `string` ([Campaign Timeframe Start](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-campaign-timeframe-start.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-campaign-timeframe-start.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/start")

### start Type

`string` ([Campaign Timeframe Start](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-campaign-timeframe-start.md))

### start Constraints

**date time**: the string must be a date time string, according to [RFC 3339, section 5.6](https://tools.ietf.org/html/rfc3339 "check the specification")

### start Examples

```json
"2018-11-13T20:20:39+00:00"
```

```json
"2020-10-01T00:00:00-05:00"
```

## end

Campaigns run for a certain time. Measurements trying to be done for this campaign after this point in time will be ignored.

`end`

*   is required

*   Type: `string` ([Campaign Timeframe End](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-campaign-timeframe-end.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-campaign-timeframe-end.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/end")

### end Type

`string` ([Campaign Timeframe End](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-campaign-timeframe-end.md))

### end Constraints

**date time**: the string must be a date time string, according to [RFC 3339, section 5.6](https://tools.ietf.org/html/rfc3339 "check the specification")

### end Examples

```json
"2018-11-13T20:20:39+00:00"
```

```json
"2020-12-31T23:59:59-05:00"
```

## aggregationPeriodLength

The length of the aggregation period in number of days. At the end of a period, the aggregated data will be sent to the analytics server.

`aggregationPeriodLength`

*   is required

*   Type: `integer` ([Length of an Aggregation Period](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-length-of-an-aggregation-period.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-length-of-an-aggregation-period.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/aggregationPeriodLength")

### aggregationPeriodLength Type

`integer` ([Length of an Aggregation Period](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-length-of-an-aggregation-period.md))

### aggregationPeriodLength Constraints

**minimum**: the value of this number must greater than or equal to: `1`

### aggregationPeriodLength Examples

```json
1
```

```json
2
```

```json
3
```

```json
7
```

```json
14
```

```json
30
```

## numberOfPeriods

The number of periods you want to measure in a row. Therefore the total length in days you measure one user is `aggregationPeriodLength * numberOfPeriods` beginning with the first day of the next period after the user consented.

`numberOfPeriods`

*   is optional

*   Type: `integer` ([Number of Periods](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-number-of-periods.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-number-of-periods.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/numberOfPeriods")

### numberOfPeriods Type

`integer` ([Number of Periods](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-number-of-periods.md))

### numberOfPeriods Constraints

**minimum**: the value of this number must greater than or equal to: `1`

### numberOfPeriods Default Value

The default value is:

```json
1
```

### numberOfPeriods Examples

```json
1
```

```json
2
```

```json
3
```

## onlyRecordOnce

Will result in recording only the first time a visit or event happened per period. Useful for yes/no questions.

`onlyRecordOnce`

*   is optional

*   Type: `boolean` ([Only-Record-Once Flag](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-only-record-once-flag.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-only-record-once-flag.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/onlyRecordOnce")

### onlyRecordOnce Type

`boolean` ([Only-Record-Once Flag](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-only-record-once-flag.md))

## eventAggregationRule

The rule how to aggregate the value of an event (if any given) with subsequent calls. Either 'sum' or 'avg'.

`eventAggregationRule`

*   is optional

*   Type: `string` ([Event Value Aggregation Rule](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-event-value-aggregation-rule.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-event-value-aggregation-rule.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/eventAggregationRule")

### eventAggregationRule Type

`string` ([Event Value Aggregation Rule](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-event-value-aggregation-rule.md))

### eventAggregationRule Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value   | Explanation |
| :------ | :---------- |
| `"sum"` |             |
| `"avg"` |             |

### eventAggregationRule Default Value

The default value is:

```json
"sum"
```

## strengthenAnonymity

When set to true, measurements only ever start at the next full period. This ensures, that anonymity guaranties aren't accidentally reduced because the first period is very short.

`strengthenAnonymity`

*   is optional

*   Type: `boolean` ([Strengthen Anonymity](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-strengthen-anonymity.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-strengthen-anonymity.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/strengthenAnonymity")

### strengthenAnonymity Type

`boolean` ([Strengthen Anonymity](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-strengthen-anonymity.md))
