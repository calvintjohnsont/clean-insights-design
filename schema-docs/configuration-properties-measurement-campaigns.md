# Measurement Campaigns Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns
```

Each insight you want to gain needs to be configured as a measurement campaign.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## campaigns Type

`object` ([Measurement Campaigns](configuration-properties-measurement-campaigns.md))

# campaigns Properties

| Property | Type     | Required | Nullable       | Defined by                                                                                                                                                                                                                           |
| :------- | :------- | :------- | :------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `^.+$`   | `object` | Optional | cannot be null | [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$") |

## Pattern: `^.+$`

Every campaign needs a unique identifier, which you use as an argument to a measurement call, so that the measurement can be checked against the campaign parameters.

`^.+$`

*   is optional

*   Type: `object` ([Unique Campaign ID](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$")

### ^.+$ Type

`object` ([Unique Campaign ID](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id.md))

### ^.+$ Examples

```json
"my-special-campaign"
```

```json
"settingsUsage"
```
