# Override Consents for server-side Usage Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/serverSideAnonymousUsage
```

When set to true, assumes consent for all campaigns and none for features. Only use this, when you're running on the server and don't measure anything users might need to give consent to!

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## serverSideAnonymousUsage Type

`boolean` ([Override Consents for server-side Usage](configuration-properties-override-consents-for-server-side-usage.md))
